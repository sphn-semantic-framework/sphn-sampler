#!/bin/bash

poetry config repositories.gitlab "https://git.dcc.sib.swiss/api/v4/projects/630/packages/pypi"
poetry publish --build --username "$SPHN_SAMPLER_TOKEN_NAME" --password "$SPHN_SAMPLER_TOKEN_SECRET" --repository gitlab -vvv
