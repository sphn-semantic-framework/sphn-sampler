from pathlib import Path
from typing import Optional, Tuple

from rdflib import Graph

from sphn_sampler.utils import add_external_terminologies
from sphn_sampler.workflows import merge_samples, workflow_1_prep, workflow_2_query, workflow_3_to_dict
from sphn_sampler.data_checks import check_sampler_data


def main(
    ontology_file: Path,
    external_terminology_files: list[Path],
    queries_n_limit: int = 100,
    graph_pkl: Optional[Path] = None,
    data_checks: Optional[dict] = None,
) -> Tuple[dict, dict]:

    print(f"Loading {ontology_file}...")
    graph = Graph()
    graph.parse(ontology_file)

    print("Running perp...")
    df_restrictions_from_schema, df_queries = workflow_1_prep(graph, queries_n_limit)

    print("Loading external terminologies...")
    print(len(graph))
    graph = add_external_terminologies(graph, external_terminology_files, graph_pkl)
    print(len(graph))

    print("Running queries...")
    df_query_results = workflow_2_query(graph, df_queries)

    print("Exporting data...")
    subclasses_not_allowed_dict, subclasses_query_results_dict, subclasses_query_results_queries = workflow_3_to_dict(
        df_restrictions_from_schema, df_query_results
    )

    samples = merge_samples(subclasses_not_allowed_dict, subclasses_query_results_dict)

    if data_checks:
        check_sampler_data(samples, data_checks)

    return samples, subclasses_query_results_queries
