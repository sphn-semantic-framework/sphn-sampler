import click
import json
from pathlib import Path

from . import __version__
from .main import main
from .workflows import export_samples
from .data_checks import check_sampler_data

CONTEXT_SETTINGS = {"help_option_names": ["-h", "--help"]}
CACHE_DIR = Path("~/sphn-sampler-cache").expanduser()


@click.command(
    help="Generate samples for SPHN Mocker from an SPHN RDF schema.",
    context_settings=CONTEXT_SETTINGS,
)
@click.version_option(__version__)
@click.option(
    "-i",
    "--schema_file",
    type=Path,
    required=True,
    help="Input file. An SPHN-compliant RDF schema.",
)
@click.option(
    "-e",
    "--external-terminology-file",
    type=Path,
    multiple=True,
    help=".ttl file for external terminologies. Specify multiple: -e sphn_atc_2024-2016-1.ttl -e sphn_chop_2024-2013-1.ttl",
)
@click.option(
    "-s",
    "--sampler-checks-file",
    type=Path,
    help=".json file with sampler checks. If not provided, no checks will be performed.",
)
@click.option(
    "-o",
    "--out-dir",
    type=Path,
    required=True,
    help="Output directory",
)
@click.option(
    "-n",
    "--queries-n-limit",
    type=click.IntRange(
        min=1,
        min_open=False,
    ),
    default=101,
    help="Maximum number of results returned by the SPARQL queries",
    show_default=True,
)
@click.option(
    "-c",
    "--use-caching",
    is_flag=True,
    default=False,
    help=f"""Use this for development purposes only. It does not handle changing input data. If used, it will 
    cache into {CACHE_DIR}""",
    show_default=True,
)
def cli(schema_file, external_terminology_file, out_dir, queries_n_limit, use_caching, sampler_checks_file):

    external_terminology_files = external_terminology_file
    # external_terminology_files = [e.absolute() for e in external_terminology_file]

    out_dir_info = out_dir / "info"
    out_dir_info.mkdir(exist_ok=True, parents=True)

    if use_caching:
        CACHE_DIR.mkdir(exist_ok=True, parents=True)
        graph_pkl = Path(CACHE_DIR / "graph.pkl")
    else:
        graph_pkl = None

    samples, subclasses_query_results_queries = main(
        schema_file, external_terminology_files, queries_n_limit, graph_pkl
    )

    # check that samples look right
    if sampler_checks_file:
        with open(sampler_checks_file) as json_file:
            samples_checks = json.load(json_file)
        check_sampler_data(samples, samples_checks)

    export_samples(
        samples,
        subclasses_query_results_queries,
        out_dir,
        sampler_out_file="value_sets/sphn-sampler-data.json",
        queries_info_out_file="info/sphn-sampler-queries.json",
    )

    info = {
        "schema_file": str(schema_file),
        "external_terminology_files": [str(e) for e in external_terminology_files],
        "queries_n_limit": queries_n_limit,
        "use_caching": use_caching,
        "sphn_sampler_version": __version__,
    }
    info_file = out_dir_info / "info.json"
    with info_file.open("w", encoding="UTF-8") as fi:
        json.dump(info, fi, indent=4)

    if use_caching:
        print(f"CACHE_DIR {CACHE_DIR} persists. Delete when no longer needed.")
