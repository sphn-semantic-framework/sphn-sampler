from rdflib import Graph
from rdflib.query import Result
import pandas as pd
from pathlib import Path
from typing import Any, Union, Optional, Callable
import pickle


def add_external_terminologies(
    graph: Graph, external_terminology_files: list[Path], graph_pkl: Optional[Path] = None
) -> Graph:
    if graph_pkl and graph_pkl.exists():
        print(f"Loading {graph_pkl}")
        with open(graph_pkl, "rb") as file:
            graph = pickle.load(file)
    else:
        for f in external_terminology_files:
            print(f"adding {f}")
            graph.parse(f)
        if graph_pkl:
            print(f"Writing to {graph_pkl}")
            with open(graph_pkl, "wb") as file:
                pickle.dump(graph, file)
    return graph


def query_results_to_df(query_result: Result, properties: None | list[str] = None) -> pd.DataFrame:
    if not properties:
        properties = [str(v) for v in query_result.vars]
    df = pd.DataFrame()
    for i, row in enumerate(query_result):
        d = {}
        for p in properties:
            if row[p]:
                d[p] = row[p].toPython()
            else:
                d[p] = None
        df = pd.concat((df, pd.DataFrame(d, index=[i])))
    return df


def format_restriction(s: str) -> str:
    s = s.split(" ")[0].split("/")[0]
    s = s.replace("sphn:", "https://biomedit.ch/rdf/sphn-schema/sphn#")
    return s


def concat_list_entries(items):
    s = "> <".join(items)
    formatted = f"<{s}>"
    return formatted


def _try_index_from_str(s: str) -> Union[str, int]:
    try:
        return int(s)
    except ValueError:
        return s


def _nested_set(dic: dict, keys: list, value: Any) -> None:
    """
    adapted from
    https://stackoverflow.com/questions/14692690/access-nested-dictionary-items-via-a-list-of-keys
    """
    for key in keys[:-1]:
        if isinstance(dic, dict):
            dic = dic.setdefault(key, {})
        elif isinstance(dic, list):
            # skipping creation, since we assume that we don't need to create new list entries
            dic = dic[_try_index_from_str(key)]
        else:
            raise NotImplementedError(f"Not implemented for type {type(dic)}")

    dic[_try_index_from_str(keys[-1])] = value


def export_subclasses_not_allowed(df: pd.DataFrame) -> dict:
    """
    Returns:
        dict of format
        {
            "class0": {"prop0": ["value0_0", "value0_1"]},
            "class1": {"prop1": ["value1_0", "value1_1"]},
        }
    """

    d: dict = {}
    for _, row in df.iterrows():
        _nested_set(dic=d, keys=[row["class"], row["property"]], value=row["value"])
    return d


def export_subclasses_query_results(df_subclass_queries: pd.DataFrame) -> list[dict]:
    """
    Returns:
        dict of format
        {
            "class0": {"prop0": ["value0_0", "value0_1"]},
            "class1": {"prop1": ["value1_0", "value1_1"]},
        }
    """
    d: dict = {}
    queries: dict = {}
    for _, row in df_subclass_queries.iterrows():
        try:
            query_results_list = row["query_results"]["class"].to_list()
        except KeyError:
            query_results_list = []
        _nested_set(dic=d, keys=[row["class"], row["property"]], value=query_results_list)
        _nested_set(
            dic=queries,
            keys=[row["class"], row["property"]],
            value={"n_results": len(query_results_list), "query": row["query"]},
        )

    return [d, queries]


def merge_dicts(dict1, dict2):
    for key in dict2:
        if key in dict1 and isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
            merge_dicts(dict1[key], dict2[key])
        else:
            dict1[key] = dict2[key]
    return dict1


def apply_func_to_dict_keys(d: Any, func: Callable, *args: Any, **kwargs: Any) -> dict:
    """
    Applies a function to all entries in a nested dictionary (excluding the last leaves).
    """

    def apply_func(val, func, *args, **kwargs):
        try:
            return func(val, *args, **kwargs)
        except Exception:
            return val

    if not isinstance(d, dict):
        return d

    new_d = {}
    for k, v in d.items():
        new_k = apply_func(k, func, *args, **kwargs)

        if isinstance(v, dict):
            new_v = apply_func_to_dict_keys(v, func, *args, **kwargs)
        else:
            new_v = v

        new_d[new_k] = new_v
    return new_d


def fix_prefixes(d: dict) -> dict:
    def _apply_fix(s: Any, prefix_lookup: dict) -> Any:
        try:
            for pattern, replacement_pattern in prefix_lookup.items():
                if pattern in s:
                    return s.replace(pattern, replacement_pattern)
        except Exception:
            return s
        return s

    # get prefixes from dataset2rdf config
    # import yaml
    # with open("dataset2rdf/dataset2rdf/config.yaml", "r") as file:
    #     data = yaml.load(file, Loader=yaml.FullLoader)

    # d = {}
    # d[data["iri"]["canonical_iri"]] = "sphn:"
    # for _, v in data["imports"].items():
    #     p = v["prefix"]
    #     d[v["iri"]["canonical_iri"]] = f"{p}:"
    # print(d)

    prefix_lookup = {
        "https://biomedit.ch/rdf/sphn-schema/sphn#": "sphn:",
        "https://www.whocc.no/atc_ddd_index/?code=": "atc:",
        "https://biomedit.ch/rdf/sphn-resource/chop/": "chop:",
        "http://purl.obolibrary.org/obo/ECO_": "eco:",
        "http://edamontology.org/": "edam:",
        "http://www.ebi.ac.uk/efo/EFO_": "efo:",
        "https://biomedit.ch/rdf/sphn-resource/emdn/": "emdn:",
        "http://purl.obolibrary.org/obo/GENEPIO_": "genepio:",
        "http://purl.obolibrary.org/obo/GENO_": "geno:",
        "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/": "hgnc:",
        "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/": "icd-10-gm:",
        "https://loinc.org/rdf/": "loinc:",
        "http://purl.obolibrary.org/obo/OBI_": "obi:",
        "http://www.orpha.net/ORDO/": "ordo:",
        "http://snomed.info/id/": "snomed:",
        "http://purl.obolibrary.org/obo/SO_": "so:",
        "https://biomedit.ch/rdf/sphn-resource/ucum/": "ucum:",
    }
    fixed_d = apply_func_to_dict_keys(d, _apply_fix, prefix_lookup=prefix_lookup)
    return fixed_d


def df_properties_to_path(df: pd.DataFrame, sep: str = "/") -> pd.DataFrame:
    """
    Joins all columns starting with "property" with a "/" separator (or defined in sep)
    """
    col_name = "property"
    df["tmp"] = df.filter(regex=f"^{col_name}").apply(lambda x: sep.join(x), axis=1)

    df = df.drop(columns=df.filter(regex=f"^{col_name}").columns)
    df = df.rename(columns={"tmp": col_name})

    return df


def get_sparql_result_len(result: Result) -> int:
    try:
        return len(result.result)
    except Exception:
        return 0
