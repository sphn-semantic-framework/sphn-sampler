def check_sampler_data(data: dict, data_checks: dict) -> None:
    # strict checking
    for class_name, v in data_checks["strict"].items():
        if class_name not in data.keys():
            raise RuntimeError(f"{class_name} not a key in data.")

        for property_path, values in v.items():
            if property_path not in data[class_name].keys():
                raise RuntimeError(f"{class_name}: {property_path} not a key in data.")

            if (actual_data := set(data[class_name][property_path])) != set(values):
                raise RuntimeError(
                    f"{class_name}: {property_path}: values mismatch\nexpected:\n{values}\n\nfound:\n{actual_data}."
                )

    # relaxed checking (for subclasses allowed sampling)
    for class_name, v in data_checks["relaxed"].items():
        if class_name not in data.keys():
            raise RuntimeError(f"{class_name} not a key in data.")

        for property_path, values in v.items():
            if property_path not in data[class_name].keys():
                raise RuntimeError(f"{class_name}: {property_path} not a key in data.")

            missing_values = set(values) - set((actual_data := data[class_name][property_path]))
            if missing_values:
                raise RuntimeError(
                    f"{class_name}: {property_path}: values missing\nmissing:\n{missing_values}\n\nfound:\n{actual_data}."
                )
