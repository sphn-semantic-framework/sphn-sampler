import json
from pathlib import Path

import pandas as pd
from rdflib import Graph

from sphn_sampler.utils import (
    concat_list_entries,
    export_subclasses_not_allowed,
    export_subclasses_query_results,
    fix_prefixes,
    format_restriction,
    merge_dicts,
    query_results_to_df,
    df_properties_to_path,
    get_sparql_result_len,
)


def query_restrictions(graph: Graph) -> pd.DataFrame:
    """
    samples restrictions up to 3 levels deep

    e.g.,
    sphn:c3 a owl:Class ;
        rdfs:subClassOf [ a owl:Class ;
                owl:intersectionOf ( [ a owl:Restriction ;
                            owl:onProperty sphn:hasQuantity ;
                            owl:someValuesFrom [ a owl:Restriction ;
                                    owl:onProperty sphn:hasUnit ;
                                    owl:someValuesFrom [ a owl:Class ;
                                            owl:unionOf ( [ a owl:Restriction ;
                                                        owl:hasValue ucum:min ;
                                                        owl:onProperty sphn:hasCode ]
    will return
    sphn:c3, sphn:hasQuantity/sphn:hasUnit/sphn:hasCode, ucum:min
    """

    queries = [
        """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX sphn: <https://biomedit.ch/rdf/sphn-schema/sphn#>

    SELECT ?class ?property1 ?value
    WHERE {
    ?class (rdfs:subClassOf/owl:intersectionOf) ?restrictionList.
    ?restrictionList ((rdf:rest*)/rdf:first) ?restriction1.
    ?restriction1 owl:onProperty ?property1.
    ?restriction1 owl:someValuesFrom/(owl:unionOf/(rdf:rest*)/rdf:first)* ?value.
    FILTER(ISIRI(?value)).
    FILTER(!strstarts(str(?value), "http://www.w3.org/2001/XMLSchema#"))
    }
    GROUP BY ?class ?property1 ?value
    ORDER BY ?class ?property1 ?value
    """,
        """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX sphn: <https://biomedit.ch/rdf/sphn-schema/sphn#>

    SELECT ?class ?property1 ?property2 ?value
    WHERE {
    ?class (rdfs:subClassOf/owl:intersectionOf) ?restrictionList.
    ?restrictionList ((rdf:rest*)/rdf:first) ?restriction1.
    ?restriction1 owl:onProperty ?property1.
    ?restriction1 owl:someValuesFrom ?restriction2.
    ?restriction2 owl:onProperty ?property2.
    ?restriction2 owl:someValuesFrom+/(owl:unionOf/(rdf:rest*)/rdf:first)* ?value.
    FILTER(ISIRI(?value)).
    FILTER(!strstarts(str(?value), "http://www.w3.org/2001/XMLSchema#"))
    }
    GROUP BY ?class ?property1 ?property2 ?value
    ORDER BY ?class ?property1 ?property2 ?value

    """,
        """
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX sphn: <https://biomedit.ch/rdf/sphn-schema/sphn#>

    SELECT ?class ?property1 ?property2 ?property3 ?value
    WHERE {
    ?class (rdfs:subClassOf/owl:intersectionOf) ?restrictionList.
    ?restrictionList ((rdf:rest*)/rdf:first) ?restriction1.
    ?restriction1 owl:onProperty ?property1.
    ?restriction1 owl:someValuesFrom ?restriction2.
    ?restriction2 owl:onProperty ?property2.
    ?restriction2 owl:someValuesFrom/(owl:unionOf/(rdf:rest*)/rdf:first)* ?restriction3.
    ?restriction3 owl:onProperty ?property3.
    ?restriction3 owl:hasValue ?value
    FILTER(ISIRI(?value)).
    FILTER(!strstarts(str(?value), "http://www.w3.org/2001/XMLSchema#"))
    }
    GROUP BY ?class ?property1 ?property2 ?property3 ?value
    ORDER BY ?class ?property1 ?property2 ?property3 ?value

    """,
    ]
    dfs = []
    for query in queries:
        query_result = graph.query(query)
        df = query_results_to_df(query_result)
        df = df_properties_to_path(df, sep="|")
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    return df


def get_property_value_restrictions(graph: Graph) -> pd.DataFrame:
    """
    get all restrictions (owl:someValuesFrom) on properties
    query returns (class, property, value)
    """

    df_named_individuals = get_named_individuals(graph)

    df_prop_value_restrictions = query_restrictions(graph)

    # filter out named individuals (value sets) before generating queries
    df_prop_value_restrictions_grouped = pd.DataFrame(
        df_prop_value_restrictions.groupby(["class", "property"])["value"].apply(list)
    )
    ind = df_prop_value_restrictions_grouped.value.apply(len) == 1
    df_prop_value_restrictions_grouped["value_single"] = pd.NA
    df_prop_value_restrictions_grouped.loc[ind, "value_single"] = df_prop_value_restrictions_grouped["value"].str[0]

    df_prop_value_restrictions_grouped = pd.merge(
        df_prop_value_restrictions_grouped.reset_index(),
        df_named_individuals,
        left_on="value_single",
        right_on="class",
        how="left",
        indicator=True,
    )
    ind = df_prop_value_restrictions_grouped._merge != "both"
    df_prop_value_restrictions_grouped = df_prop_value_restrictions_grouped[ind]

    df_prop_value_restrictions_grouped = df_prop_value_restrictions_grouped.drop(
        columns=["value_single", "individual", "_merge"]
    ).set_index(["class", "property"])
    return df_prop_value_restrictions_grouped


def get_skos_restrictions(graph: Graph) -> pd.DataFrame:
    """
    query returns (class, restriction) of all instances of skos:definition
    <class> skos:definition <restriction>

    the list is filtered to only return notes that indicate subclasses not being allowed
    """

    query = """PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX sphn: <https://biomedit.ch/rdf/sphn-schema/sphn#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    
    SELECT ?class ?restriction
    WHERE {
        ?class a owl:Class.
        ?class skos:scopeNote ?restriction.
        FILTER (STRSTARTS(str(?class), str(sphn:)))
    }
    ORDER BY ?class ?restriction
    """
    query_result = graph.query(query)
    if get_sparql_result_len(query_result) > 0:
        df_restrictions = query_results_to_df(query_result, properties=["class", "restriction"])
        ind = df_restrictions.restriction.str.match("[A-Za-z:/]+ (no subclasses|subclasses not) allowed")
        df_restrictions = df_restrictions[ind]

        df_restrictions["property"] = df_restrictions.restriction.apply(format_restriction)
    else:
        df_restrictions = pd.DataFrame([], columns=["class", "restriction", "property"])
    return df_restrictions


def get_subclasses_not_allowed(
    df_prop_value_restrictions: pd.DataFrame, df_restrictions: pd.DataFrame
) -> pd.DataFrame:
    """
    returns instances of value restrictions (someValuesFrom) where subclasses are not allowed
    (hence no further query required)
    """
    # we need to account for the fact that we need to restirict on properties of type root_prop/prop2/prop3, but the
    # skos:scopeNote only has the root_prop
    df_prop_value_restrictions = df_prop_value_restrictions.reset_index()
    df_prop_value_restrictions["root_property"] = df_prop_value_restrictions.property.str.split("|").str[0]

    # join df with restrictions
    df_restrictions = df_restrictions[["class", "property"]]
    df_restrictions_from_schema = (
        pd.merge(
            df_prop_value_restrictions,
            df_restrictions,
            left_on=["class", "root_property"],
            right_on=["class", "property"],
            how="inner",
        )
        .drop(columns=["property_y", "root_property"])
        .rename(columns={"property_x": "property"})
    )

    return df_restrictions_from_schema


def get_queries(
    df_prop_value_restrictions: pd.DataFrame,
    df_restrictions: pd.DataFrame,
    df_type: pd.DataFrame,
    queries_n_limit: int,
) -> pd.DataFrame:
    """
    takes instances of value restrictions (someValuesFrom) where subclasses are allowed
    generates queries that
    - gets the parents of the values
    - gets all subclasses of the parents
    - returns the subclasses (and labels)

    treats ucum as special case (injected via df_type); builds a query that returns all instances of rdf:type value
    """

    def _render_subclass_query_template(items: str, queries_n_limit: int, subclass_queries_query_template: str) -> str:
        subclass_queries_query_template = subclass_queries_query_template.replace(
            "LIMIT_PLACEHOLDER", f"LIMIT {queries_n_limit}"
        )
        concatenated_values = concat_list_entries(items)
        rendered_query = subclass_queries_query_template.replace("VALUES_PLACEHOLDER", concatenated_values)
        return rendered_query

    subclass_queries_query_template = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?class ?label WHERE {
    VALUES ?parent { VALUES_PLACEHOLDER }
    ?class rdfs:subClassOf* ?parent .
    ?class rdfs:label ?label
    FILTER (!isBlank(?class))
} LIMIT_PLACEHOLDER
"""

    tmp_df = pd.merge(
        df_prop_value_restrictions,
        df_restrictions.drop(columns=["value"]),
        on=["class", "property"],
        how="left",
        indicator=True,
    )

    anti_join = tmp_df[tmp_df["_merge"] == "left_only"].drop(columns=["_merge"])

    if len(anti_join) > 0:
        anti_join["query"] = anti_join.value.apply(
            _render_subclass_query_template,
            args=(queries_n_limit, subclass_queries_query_template),
        )

    else:
        anti_join["query"] = None
    subclass_queries = anti_join.drop(columns=["value"])

    # workaround since UCUM does not have subclasses: look for "rdf:type <https://biomedit.ch/rdf/sphn-resource/ucum/UCUM"
    def _render_type_query_template(rdf_type: list, queries_n_limit: int, type_queries_query_template: str) -> str:
        rdf_type, *rest = rdf_type
        if rest:
            raise NotImplementedError("only implemented for list of len 1")

        type_queries_query_template = type_queries_query_template.replace(
            "LIMIT_PLACEHOLDER", f"LIMIT {queries_n_limit}"
        )
        rendered_query = type_queries_query_template.replace("TYPE_PLACEHOLDER", rdf_type)
        return rendered_query

    type_queries_query_template = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?class ?label WHERE {
    ?class a <TYPE_PLACEHOLDER> .
    ?class rdfs:label ?label
    FILTER (!isBlank(?class))
} LIMIT_PLACEHOLDER
"""

    df_type["query"] = df_type.value.apply(
        _render_type_query_template, args=(queries_n_limit, type_queries_query_template)
    )
    df_type = df_type.reset_index().drop(columns=["value"])

    queries = pd.concat((subclass_queries, df_type), ignore_index=True)

    return queries


def get_named_individuals(graph: Graph) -> pd.DataFrame:
    query = """PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>

    SELECT ?class ?individual (GROUP_CONCAT(?label;SEPARATOR=", ") AS ?labels)
    WHERE { 
        ?individual rdf:type owl:NamedIndividual.
        ?individual rdf:type ?class.
        OPTIONAL { ?individual rdfs:label ?label. }
        FILTER (?class != owl:NamedIndividual)
    }
    GROUP BY ?class ?individual
    ORDER BY ?class ?individual
    """

    query_result = graph.query(query)
    if get_sparql_result_len(query_result) > 0:
        df_named_individuals = query_results_to_df(query_result, properties=["class", "individual"])
        df_named_individuals_grouped = df_named_individuals.groupby(["class"])["individual"].apply(list)

        df_named_individuals = pd.DataFrame(df_named_individuals_grouped.str[0])
    else:  # empty query result
        df_named_individuals = pd.DataFrame(columns=["individual"])
        df_named_individuals.index.name = "class"
    return df_named_individuals


def _format_property_path_sep(df: pd.DataFrame) -> pd.DataFrame:
    df["property"] = df["property"].str.replace("|", "/", regex=False)
    return df


def filter_types(df_prop_value_restrictions: pd.DataFrame, rdf_type: str) -> tuple[pd.DataFrame, pd.DataFrame]:
    # split ucum values off from prop value restrictions
    def _is_type(x):
        return rdf_type in x

    type_ind = df_prop_value_restrictions["value"].apply(_is_type)
    df_type = df_prop_value_restrictions[type_ind].copy()
    df_prop_value_restrictions = df_prop_value_restrictions[~type_ind].copy()
    return df_type, df_prop_value_restrictions


def workflow_1_prep(graph: Graph, queries_n_limit: int) -> tuple[pd.DataFrame, pd.DataFrame]:
    df_prop_value_restrictions = get_property_value_restrictions(graph)
    rdf_type = "https://biomedit.ch/rdf/sphn-resource/ucum/UCUM"
    df_type, df_prop_value_restrictions = filter_types(df_prop_value_restrictions, rdf_type)

    df_skos_restrictions = get_skos_restrictions(graph)

    df_restrictions_from_schema = get_subclasses_not_allowed(df_prop_value_restrictions, df_skos_restrictions)

    df_queries = get_queries(df_prop_value_restrictions, df_restrictions_from_schema, df_type, queries_n_limit)
    df_restrictions_from_schema = _format_property_path_sep(df_restrictions_from_schema)
    df_queries = _format_property_path_sep(df_queries)
    return (df_restrictions_from_schema, df_queries)


def workflow_2_query(graph: Graph, df_queries: pd.DataFrame) -> pd.DataFrame:
    def _run_query(query: str, graph: Graph) -> pd.DataFrame:
        query_results = graph.query(query)
        df = query_results_to_df(query_result=query_results, properties=["class", "label"])
        return df

    df_queries["query_results"] = df_queries["query"].apply(_run_query, args=(graph,))
    return df_queries


def workflow_3_to_dict(df_restrictions_from_schema: pd.DataFrame, df_queries: pd.DataFrame) -> list[dict]:
    subclasses_not_allowed_dict = export_subclasses_not_allowed(df_restrictions_from_schema)

    query_results_dict, query_results_queries = export_subclasses_query_results(df_queries)

    return [subclasses_not_allowed_dict, query_results_dict, query_results_queries]


def merge_samples(
    subclasses_not_allowed_dict: dict,
    subclasses_query_results_dict: dict,
) -> dict:
    """
    subclasses_not_allowed_dict and subclasses_query_results_dict are combined in a dict with the following format
    {
        "class_name_0": {
            "property_0_0": {["value_0_0_0", "value_0_0_1"]},
            "property_0_1": {["value_0_1_0", "value_0_1_1"]},
        },
        "class_name_1": {
            "property_1_0": {["value_1_0_0", "value_1_0_1"]},
            "property_1_1": {["value_1_1_0", "value_1_1_1"]},
        },
    }

    """
    samples = merge_dicts(subclasses_not_allowed_dict, subclasses_query_results_dict)

    # replace entry that have prefixes defined
    samples = fix_prefixes(samples)

    return samples


def export_samples(
    samples: dict,
    subclasses_query_results_queries: dict,
    out_dir: Path,
    sampler_out_file: str = "sphn-sampler-data.json",
    queries_info_out_file: str = "sphn-sampler-queries.json",
) -> None:
    file = out_dir / sampler_out_file
    file.parent.mkdir(parents=True, exist_ok=True)
    with open(file, "w") as json_file:
        json.dump(samples, json_file, indent=4)

    file = out_dir / queries_info_out_file
    with open(file, "w") as json_file:
        json.dump(subclasses_query_results_queries, json_file, indent=4)
