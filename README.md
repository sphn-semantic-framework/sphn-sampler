# SPHN Sampler

The SPHN Sampler utilizes a given schema and external terminologies
to generate sample data for specific properties.

For properties where descendants or subclasses of the specified classes are not allowed,
the tool returns a list of allowed values (such as named individuals from value sets
or entities from external terminologies). For properties where descendants
or subclasses of the specified classes are allowed, it samples from the class
and its subclasses within the external terminology.

## Prerequisites

### Install Python

The package requires Python 3.10 or later to be installed on the system.

### Create a virtual environment (optional)

It is recommended to install the package into a virtual environment, such as one created with
[`venv`](https://docs.python.org/3/library/venv.html) (as shown in the example)
or with [Conda](https://docs.conda.io/).

```bash
python -m venv .venv
source .venv/bin/activate
```

## Installation

### Using pip

Ensure pip is installed and up-to-date:

```bash
python -m ensurepip --upgrade
```

Install the sphn-sampler package:

```bash
pip install sphn-sampler --index-url https://git.dcc.sib.swiss/api/v4/projects/630/packages/pypi/simple
```

Run the tool:

```bash
sphn-sampler -h
```

### Using Poetry

Clone the repository.

```bash
git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sampler.git
cd sphn-sampler
```

Install the package:

```bash
poetry install
```

To run the tool, use `poetry run`:

```bash
poetry run sphn-sampler -h
```

## Usage

```bash
$ sphn-sampler -h
Usage: sphn-sampler [OPTIONS]

  Generate samples for SPHN Mocker from an SPHN RDF schema.

Options:
  --version                       Show the version and exit.
  -i, --schema_file PATH          Input file. An SPHN-compliant RDF schema.
                                  [required]
  -e, --external-terminology-file PATH
                                  .ttl file for external terminologies.
                                  Specify multiple: -e
                                  sphn_atc_2024-2016-1.ttl -e
                                  sphn_chop_2024-2013-1.ttl
  -s, --sampler-checks-file PATH  .json file with sampler checks. If not
                                  provided, no checks will be performed.
  -o, --out-dir PATH              Output directory  [required]
  -n, --queries-n-limit INTEGER RANGE
                                  Maximum number of results returned by the
                                  SPARQL queries  [default: 101; x>=1]
  -c, --use-caching               Use this for development purposes only. It
                                  does not handle changing input data. If
                                  used, it will  cache into
                                  /Users/otopalov/sphn-sampler-cache
  -h, --help                      Show this message and exit.
```

## Creating samples for SPHN Mocker

### Prepare the files

In this example, we create an empty directory named `sampler` and copy the external terminologies,
the SPHN RDF Schema, and a sampler-checks file into this directory.

```bash
$ tree sampler
sampler
├── sampler_checks.json
└── sphn_rdf_schema.ttl
├── terminologies
│   ├── snomed-ct-CH-20231201.ttl
│   ├── sphn_atc_2024-2016-1.ttl
│   ├── sphn_chop_2024-2013-1.ttl
...
│   └── sphn_ucum_2024-1.ttl
```

#### The sampler_checks.json file

This (optional) file allows you to add checks to the output samples.
It has two modes: **strict** and **relaxed**.

- The strict mode can be used for cases where subsamples are not allowed
  and the number of resulting samples is manageable.
  For those cases, the tool will check if the resulting samples look exactly like the checks.

- The relaxed mode, meant for cases where subsamples are allowed,
  only checks that the entities from the checks are present in the samples.
  If there are superfluous entities in the samples, those are ignored.

_Example_

```json
{
    "strict": {
        "sphn:AdministrativeSex": {
            "sphn:hasCode": [
                "http://snomed.info/id/248152002",
                "http://snomed.info/id/248153007",
                "http://snomed.info/id/32570681000036106"
            ]
        },
        "sphn:TumorSpecimen": {
            "sphn:hasTumorPurity/sphn:hasUnit/sphn:hasCode": [
                "https://biomedit.ch/rdf/sphn-resource/ucum/percent"
            ]
        }
    },
    "relaxed": {
        "sphn:Allergen": {
            "sphn:hasCode": [
                "http://snomed.info/id/138875005",
                "http://snomed.info/id/362981000",
                "http://snomed.info/id/394730007",
 ...
            ]
        },
    }
```

### Create samples

```bash
$ sphn-sampler \
-i sphn_rdf_schema.ttl \
-e terminologies/snomed-ct-CH-20231201.ttl \
-e terminologies/sphn_atc_2024-2016-1.ttl \
-e terminologies/sphn_chop_2024-2013-1.ttl \
-e terminologies/sphn_eco_20230903-1.ttl \
-e terminologies/sphn_edam_1.25-1.ttl \
-e terminologies/sphn_efo_3.61.0-1.ttl \
-e terminologies/sphn_emdn_2021-09-29-1.ttl \
-e terminologies/sphn_genepio_20230819-1.ttl \
-e terminologies/sphn_geno_20231008-1.ttl \
-e terminologies/sphn_hgnc_20231215-1.ttl \
-e terminologies/sphn_icd-10-gm_2024-2013-1.ttl \
-e terminologies/sphn_loinc_2.76-1.ttl \
-e terminologies/sphn_obi_20230920-1.ttl \
-e terminologies/sphn_ordo_4.4-1.ttl \
-e terminologies/sphn_so_20211122-1.ttl \
-e terminologies/sphn_ucum_2024-1.ttl \
-o output
```

### Output

- `{out_dir}/value_sets/sphn-sampler-data.json` contains the samples required for SPHN Mocker.
  The entire `{out_dir}/value_sets/` directory should be copied to the `sphn-mocker` repository at
  `sphn-mocker/sphn_mocker/files/{schema_tag}/samples`.
- The `{out_dir}/info` directory contains supplementary information that, while not strictly necessary,
  is useful for tracking the provenance of the samples. It is therefore recommended to copy this directory
  into the repository as well.

## Development

### Release

When you have committed all changes and are ready to release the package, follow these steps:

1. Add release information at the bottom of this page.
2. Bump the version using Poetry.
3. Run `tag_repo.sh`.
4. Push the tags
5. Build the package and publish it at https://git.dcc.sib.swiss/groups/sphn-semantic-framework/-/packages.

```bash
poetry version patch # allowed strategies: patch, minor, major, prepatch, preminor, premajor, prerelease
bash tag_repo.sh
git push && git push --tags

# make sure to set SPHN_SAMPLER_TOKEN_NAME and SPHN_SAMPLER_TOKEN_SECRET with a token that can write to the gitlab repo.
bash ./tmp-publish.sh
```

### Memory profiling

```bash
poetry run mprof run --interval 1 run.py
poetry run mprof plot
```

#### Example

Run on `dbfc425`
![](mem_prof.png)

## Changes

- `0.5.1` updated package dependencies
- `0.5.0`
  - added support for SPHN RDF Schema 2024.2
  - renamed project
- `0.4.1` added cli
- `0.4.0` fix ucum sampling
- `0.3.0` fixing sampling strategy (skos:definition -> skos:scopeNote)
- `0.2.0` included nested paths in restriction sampling
- `0.1.1` control data out files
- `0.1.0` initial

## License and Copyright

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics

The SPHN Sampler is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) (see [License](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sampler/-/blob/main/LICENSE)).

## Contact

For any question or comment, please contact the Data Coordination Center FAIR Data team at [fair-data-team@sib.swiss](mailto:fair-data-team@sib.swiss).
