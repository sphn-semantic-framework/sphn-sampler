#!/bin/bash

v=$(poetry version -s)
echo
echo

echo Commiting version bump
message=$(echo "Version bump to $v")
git add pyproject.toml && git commit -m "$message"

echo Tagging $v
git tag $v

