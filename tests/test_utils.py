from sphn_sampler.utils import (
    format_restriction,
    concat_list_entries,
    export_subclasses_not_allowed,
    export_subclasses_query_results,
    merge_dicts,
    apply_func_to_dict_keys,
    fix_prefixes,
    df_properties_to_path,
)
import pandas as pd


def test_format_restriction_1():
    input = "sphn:hasCode subclasses not allowed"
    expected = "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode"
    assert format_restriction(input) == expected


def test_format_restriction_2_path():
    input = "sphn:hasBodySite/sphn:hasCode no subclasses allowed"
    expected = "https://biomedit.ch/rdf/sphn-ontology/sphn#hasBodySite"
    assert format_restriction(input) == expected


def test_concat_list_entries():
    l = ["x", "y", "z"]
    expected = "<x> <y> <z>"
    result = concat_list_entries(l)
    assert result == expected


def test_subclasses_not_allowed_df_to_dict():

    d = {
        "class": {
            0: "a",
            1: "b",
            2: "b",
        },
        "property": {
            0: "x",
            1: "y",
            2: "z",
        },
        "value": {
            0: range(2),
            1: range(3),
            2: range(4),
        },
    }
    df = pd.DataFrame(d)
    expected = {"a": {"x": range(2)}, "b": {"y": range(3), "z": range(4)}}
    d = export_subclasses_not_allowed(df)
    assert d == expected


def test_export_subclasses_query_results():

    results_0 = pd.DataFrame(
        {
            "class": {0: "c_i0_0"},
            "label": {0: "l_i0_0"},
        }
    )
    results_1 = pd.DataFrame(
        {
            "class": {0: "c_i1_0", 1: "c_i1_1"},
            "label": {
                0: "l_i1_0",
                1: "l_i1_1",
            },
        }
    )
    df = pd.DataFrame(
        {
            "class": {
                0: "c0",
                1: "c1",
            },
            "property": {
                0: "p0",
                1: "p1",
            },
            "query": {0: "QUERY_0", 1: "QUERY_1"},
            "query_results": {0: results_0, 1: results_1},
        }
    )

    expected = {
        "c0": {"p0": ["c_i0_0"]},
        "c1": {"p1": ["c_i1_0", "c_i1_1"]},
    }
    d, queries = export_subclasses_query_results(df)
    assert d == expected


def test_merge_dicts():
    d1 = {"level1": {"level2": {"level3": {"key1": "value1", "key2": "value2"}}}}
    d2 = {"level1": {"level2": {"level3": {"key2": "new_value2", "key3": "new_value3"}}}}

    expected = {"level1": {"level2": {"level3": {"key1": "value1", "key2": "new_value2", "key3": "new_value3"}}}}
    d = merge_dicts(d1, d2)
    assert d == expected

    d = merge_dicts(d1, d2)
    assert d == d1

    d = merge_dicts(d1, {})
    assert d == d1


def test_apply_func_to_dict():
    d = {
        "a": "foo",
        "b": {
            "c": "bar",
            "d": {
                "e": "baz",
                "f": ["blup", 23],
            },
        },
        1: "ha",
    }
    expected = {
        "A": "foo",
        "B": {
            "C": "bar",
            "D": {
                "E": "baz",
                "F": ["blup", 23],
            },
        },
        1: "ha",
    }
    res = apply_func_to_dict_keys(d, str.upper)
    assert res == expected


def test_fix_prefixes():
    d = {
        "https://biomedit.ch/rdf/sphn-ontology/sphn#AdministrativeGender": {
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode": [
                "a",
                "b",
                1,
                2,
            ]
        },
        "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy": {
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasReactionTypeCode": [
                "c",
                "d",
                3,
            ],
        },
    }
    expected = {
        "sphn:AdministrativeGender": {
            "sphn:hasCode": [
                "a",
                "b",
                1,
                2,
            ]
        },
        "sphn:Allergy": {
            "sphn:hasReactionTypeCode": [
                "c",
                "d",
                3,
            ],
        },
    }

    res = fix_prefixes(d)
    assert res == expected


def test_df_properties_to_path_1_col():
    df = pd.DataFrame({"class": ["A", "B"], "property1": ["bli", "bla"]})
    df_expected = pd.DataFrame({"class": ["A", "B"], "property": ["bli", "bla"]})

    df_res = df_properties_to_path(df)
    pd.testing.assert_frame_equal(df_res, df_expected)


def test_df_properties_to_path_2_col():
    df = pd.DataFrame({"class": ["A", "B"], "property1": ["bli", "bla"], "property2": ["ha", "hi"]})
    df_expected = pd.DataFrame({"class": ["A", "B"], "property": ["bli/ha", "bla/hi"]})

    df_res = df_properties_to_path(df)
    pd.testing.assert_frame_equal(df_res, df_expected)
