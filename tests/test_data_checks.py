import pytest

from sphn_sampler.data_checks import check_sampler_data


def test_class_name_missing():
    data = {}
    data_checks = {"strict": {"k": "v"}, "relaxed": {}}
    with pytest.raises(Exception):
        check_sampler_data(data, data_checks)


def test_missing_property_path():
    data = {"k": "v1"}
    data_checks = {"strict": {"k": "v2"}, "relaxed": {}}
    with pytest.raises(Exception):
        check_sampler_data(data, data_checks)


def test_values_mismatch():
    data = {"k": {"p2/p3": [1, 2, 3]}}
    data_checks = {"strict": {"k": {"p2/p3": [1, 2]}}, "relaxed": {}}
    with pytest.raises(Exception):
        check_sampler_data(data, data_checks)


def test_success():
    data = {"k": {"p2/p3": [1, 2, 3]}}
    data_checks = {"strict": {"k": {"p2/p3": [1, 2, 3]}}, "relaxed": {}}
    check_sampler_data(data, data_checks)

def test_success_reorder():
    data = {"k": {"p2/p3": [1, 2, 3]}}
    data_checks = {"strict": {"k": {"p2/p3": [2, 3, 1]}}, "relaxed": {}}
    check_sampler_data(data, data_checks)


def test_relaxed_list_value_missing():
    data = {"k": {"p2/p3": [1, 2, 3]}}
    data_checks = {"relaxed": {"k": {"p2/p3": [1, 2, 4]}}, "strict": {}}
    with pytest.raises(Exception):
        check_sampler_data(data, data_checks)


def test_relaxed_list_value_exist():
    data = {"k": {"p2/p3": [1, 2, 3]}}
    data_checks = {"relaxed": {"k": {"p2/p3": [1, 2]}}, "strict": {}}
    check_sampler_data(data, data_checks)
