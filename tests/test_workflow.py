from pathlib import Path
from rdflib import Graph
import pandas as pd
from sphn_sampler.workflows import (
    get_property_value_restrictions,
    get_skos_restrictions,
    workflow_1_prep,
    workflow_2_query,
    query_restrictions,
)
from sphn_sampler.main import main

data_dir = Path(__file__).parent / "data"
ontology_file = data_dir / "test_onto.ttl"
external_term_file = data_dir / "test_external_term.ttl"
nested_restrictions_file = data_dir / "nested_restrictions.ttl"
nested_restrictions_string_file = data_dir / "nested_restrictions_string.ttl"
circumference_file = data_dir / "circumference.ttl"

onto = Graph()
onto.parse(ontology_file)
onto.parse(external_term_file)


def test_get_property_value_restrictions():
    data = {
        "class": {
            0: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdministrativeGender",
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            3: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            4: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            5: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            6: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            7: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            8: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            9: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            10: "https://biomedit.ch/rdf/sphn-ontology/sphn#Intent",
        },
        "property": {
            0: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasOutcome",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasReactionTypeCode",
            3: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasSeverityCode",
            4: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasSubstanceCategory",
            5: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasVerificationStatusCode",
            6: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCertaintyCode",
            7: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasExposureRouteCode",
            8: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasManifestationCode",
            9: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasManifestationSeverityCode",
            10: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
        },
        "value": {
            0: [
                "http://snomed.info/id/261665006",
                "http://snomed.info/id/703117000",
                "http://snomed.info/id/703118005",
                "http://snomed.info/id/74964007",
            ],
            1: ["https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_outcome"],
            2: [
                "http://snomed.info/id/261665006",
                "http://snomed.info/id/419199007",
                "http://snomed.info/id/782197009",
            ],
            3: [
                "http://snomed.info/id/723505004",
                "http://snomed.info/id/723507007",
                "http://snomed.info/id/723509005",
            ],
            4: ["https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy_substanceCategory"],
            5: [
                "http://snomed.info/id/410605003",
                "http://snomed.info/id/415684004",
                "http://snomed.info/id/723510000",
                "http://snomed.info/id/723511001",
            ],
            6: [
                "http://snomed.info/id/410592001",
                "http://snomed.info/id/410605003",
                "http://snomed.info/id/415684004",
            ],
            7: ["http://snomed.info/id/284009009"],
            8: ["http://snomed.info/id/404684003"],
            9: ["http://snomed.info/id/24484000", "http://snomed.info/id/255604002", "http://snomed.info/id/6736007"],
            10: ["http://snomed.info/id/363675004"],
        },
    }
    expected = pd.DataFrame(data)
    df = get_property_value_restrictions(onto)
    pd.testing.assert_frame_equal(df.reset_index(), expected)


def test_get_skos_restrictions():
    data = {
        "class": {
            0: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdministrativeGender",
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            3: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            4: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            5: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
        },
        "restriction": {
            0: "sphn:hasCode subclasses not allowed",
            1: "sphn:hasReactionTypeCode no subclasses allowed",
            2: "sphn:hasSeverityCode no subclasses allowed",
            3: "sphn:hasVerificationStatusCode no subclasses allowed",
            4: "sphn:hasCertaintyCode no subclasses allowed",
            5: "sphn:hasManifestationSeverityCode no subclasses allowed",
        },
        "property": {
            0: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasReactionTypeCode",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasSeverityCode",
            3: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasVerificationStatusCode",
            4: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCertaintyCode",
            5: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasManifestationSeverityCode",
        },
    }
    expected = pd.DataFrame(data)
    df_restrictions = get_skos_restrictions(onto)
    test_cols = ["class", "property"]
    pd.testing.assert_frame_equal(df_restrictions[test_cols], expected[test_cols])


def test_get_subclass_queries():
    data = {
        "class": {
            0: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent",
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            3: "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode",
            4: "https://biomedit.ch/rdf/sphn-ontology/sphn#Intent",
        },
        "property": {
            0: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasOutcome",
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasSubstanceCategory",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasExposureRouteCode",
            3: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasManifestationCode",
            4: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
        },
        "query": {
            0: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_outcome> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            1: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy_substanceCategory> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            2: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <http://snomed.info/id/284009009> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            3: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <http://snomed.info/id/404684003> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            4: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <http://snomed.info/id/363675004> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
        },
    }
    expected = pd.DataFrame(data)
    df_restrictions_from_schema, df_subclass_queries = workflow_1_prep(onto, 101)

    pd.testing.assert_frame_equal(df_subclass_queries.reset_index(drop=True), expected)


def test_workflow_2_query():
    data = {
        "class": {
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent",
            5: "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy",
        },
        "property": {
            1: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasConsequences",
            2: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasOutcome",
            5: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasSubstanceCategory",
        },
        "query": {
            1: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_consequences> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            2: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_outcome> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            5: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy_substanceCategory> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
        },
    }

    df_subclass_queries = pd.DataFrame(data)

    expected = pd.DataFrame(
        {
            "class": {1: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent"},
            "property": {1: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasConsequences"},
            "query": {
                1: "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_consequences> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n"
            },
            "query_results": {
                1: pd.DataFrame(
                    {
                        "class": {0: "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_consequences"},
                        "label": {0: "Adverse Event consequences"},
                    }
                )
            },
        }
    )

    df_subclass_query_results = workflow_2_query(onto, df_subclass_queries)
    pd.testing.assert_frame_equal(df_subclass_query_results.iloc[:1], expected)


def test_workflow_2_query_external_terminology():
    _, df_subclass_queries = workflow_1_prep(onto, 101)
    # test data only has values  for last query
    df_subclass_queries = df_subclass_queries.loc[4:]
    df_subclass_query_results = workflow_2_query(onto, df_subclass_queries)

    expected_selected = [
        "http://snomed.info/id/363675004",
        "http://snomed.info/id/429892002",
        "http://snomed.info/id/447295008",
    ]
    results_selected = df_subclass_query_results.iloc[0].query_results["class"].to_list()
    assert results_selected == expected_selected


def test_end_to_end():
    expected_samples = {
        "sphn:AdministrativeGender": {
            "sphn:hasCode": [
                "http://snomed.info/id/261665006",
                "http://snomed.info/id/703117000",
                "http://snomed.info/id/703118005",
                "http://snomed.info/id/74964007",
            ]
        },
        "sphn:Allergy": {
            "sphn:hasReactionTypeCode": [
                "http://snomed.info/id/261665006",
                "http://snomed.info/id/419199007",
                "http://snomed.info/id/782197009",
            ],
            "sphn:hasSeverityCode": [
                "http://snomed.info/id/723505004",
                "http://snomed.info/id/723507007",
                "http://snomed.info/id/723509005",
            ],
            "sphn:hasVerificationStatusCode": [
                "http://snomed.info/id/410605003",
                "http://snomed.info/id/415684004",
                "http://snomed.info/id/723510000",
                "http://snomed.info/id/723511001",
            ],
            "sphn:hasSubstanceCategory": [],
        },
        "sphn:AllergyEpisode": {
            "sphn:hasCertaintyCode": [
                "http://snomed.info/id/410592001",
                "http://snomed.info/id/410605003",
                "http://snomed.info/id/415684004",
            ],
            "sphn:hasManifestationSeverityCode": [
                "http://snomed.info/id/24484000",
                "http://snomed.info/id/255604002",
                "http://snomed.info/id/6736007",
            ],
            "sphn:hasExposureRouteCode": [],
            "sphn:hasManifestationCode": [],
        },
        "sphn:AdverseEvent": {"sphn:hasOutcome": []},
        "sphn:Intent": {
            "sphn:hasCode": [
                "http://snomed.info/id/363675004",
                "http://snomed.info/id/429892002",
                "http://snomed.info/id/447295008",
            ]
        },
    }

    expected_subclasses_query_results_queries = {
        "https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent": {
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasOutcome": {
                "n_results": 0,
                "query": "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#AdverseEvent_outcome> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            }
        },
        "https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy": {
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasSubstanceCategory": {
                "n_results": 0,
                "query": "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <https://biomedit.ch/rdf/sphn-ontology/sphn#Allergy_substanceCategory> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            }
        },
        "https://biomedit.ch/rdf/sphn-ontology/sphn#AllergyEpisode": {
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasExposureRouteCode": {
                "n_results": 0,
                "query": "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <http://snomed.info/id/284009009> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            },
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasManifestationCode": {
                "n_results": 0,
                "query": "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <http://snomed.info/id/404684003> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            },
        },
        "https://biomedit.ch/rdf/sphn-ontology/sphn#Intent": {
            "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode": {
                "n_results": 3,
                "query": "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nSELECT ?class ?label WHERE {\n    VALUES ?parent { <http://snomed.info/id/363675004> }\n    ?class rdfs:subClassOf* ?parent .\n    ?class rdfs:label ?label\n    FILTER (!isBlank(?class))\n} LIMIT 101\n",
            }
        },
    }
    samples, subclasses_query_results_queries = main(
        ontology_file=ontology_file, external_terminology_files=[external_term_file], queries_n_limit=101
    )
    assert samples == expected_samples
    assert subclasses_query_results_queries == expected_subclasses_query_results_queries


def test_nested_restrictions():
    df_expected = pd.DataFrame(
        {
            "class": {
                0: "https://biomedit.ch/rdf/sphn-ontology/sphn#c1",
                1: "https://biomedit.ch/rdf/sphn-ontology/sphn#c1",
                2: "https://biomedit.ch/rdf/sphn-ontology/sphn#c1",
                3: "https://biomedit.ch/rdf/sphn-ontology/sphn#c2",
                4: "https://biomedit.ch/rdf/sphn-ontology/sphn#c3",
                5: "https://biomedit.ch/rdf/sphn-ontology/sphn#c3",
                6: "https://biomedit.ch/rdf/sphn-ontology/sphn#c3",
                7: "https://biomedit.ch/rdf/sphn-ontology/sphn#c3",
                8: "https://biomedit.ch/rdf/sphn-ontology/sphn#c3",
                9: "https://biomedit.ch/rdf/sphn-ontology/sphn#c3",
            },
            "value": {
                0: "http://snomed.info/id/1",
                1: "http://snomed.info/id/2",
                2: "http://snomed.info/id/3",
                3: "http://snomed.info/id/10",
                4: "https://biomedit.ch/rdf/sphn-resource/ucum/a",
                5: "https://biomedit.ch/rdf/sphn-resource/ucum/d",
                6: "https://biomedit.ch/rdf/sphn-resource/ucum/h",
                7: "https://biomedit.ch/rdf/sphn-resource/ucum/min",
                8: "https://biomedit.ch/rdf/sphn-resource/ucum/mo",
                9: "https://biomedit.ch/rdf/sphn-resource/ucum/wk",
            },
            "property": {
                0: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                1: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                2: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                3: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasMedicalDevice|https://biomedit.ch/rdf/sphn-ontology/sphn#hasTypeCode",
                4: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasQuantity|https://biomedit.ch/rdf/sphn-ontology/sphn#hasUnit|https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                5: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasQuantity|https://biomedit.ch/rdf/sphn-ontology/sphn#hasUnit|https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                6: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasQuantity|https://biomedit.ch/rdf/sphn-ontology/sphn#hasUnit|https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                7: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasQuantity|https://biomedit.ch/rdf/sphn-ontology/sphn#hasUnit|https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                8: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasQuantity|https://biomedit.ch/rdf/sphn-ontology/sphn#hasUnit|https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
                9: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasQuantity|https://biomedit.ch/rdf/sphn-ontology/sphn#hasUnit|https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode",
            },
        }
    ).reset_index(drop=True)
    graph = Graph()
    graph.parse(nested_restrictions_file)
    df = query_restrictions(graph)
    pd.testing.assert_frame_equal(df, df_expected)


def test_circumference():
    df_expected = pd.DataFrame(
        {
            "class": {0: "https://biomedit.ch/rdf/sphn-ontology/sphn#CircumferenceMeasure"},
            "property": {
                0: "https://biomedit.ch/rdf/sphn-ontology/sphn#hasBodySite/https://biomedit.ch/rdf/sphn-ontology/sphn#hasCode"
            },
            "value": {
                0: [
                    "http://snomed.info/id/29836001",
                    "http://snomed.info/id/33673004",
                    "http://snomed.info/id/45048000",
                    "http://snomed.info/id/69536005",
                ]
            },
        }
    ).reset_index(drop=True)
    graph = Graph()
    graph.parse(circumference_file)
    df_restrictions_from_schema, df_subclass_queries = workflow_1_prep(graph, 101)

    pd.testing.assert_frame_equal(df_restrictions_from_schema, df_expected)
